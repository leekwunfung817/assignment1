function RandomUpperString(count) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    for (let i = 0; i < count; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}

function RandomLowerString(count) {
    var text = "";
    var possible = "abcdefghijklmnopqrstuvwxyz";

    for (let i = 0; i < count; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function RandomNumber(from, to) {
    var dif = to - from;
    return parseInt((Math.random() * dif) + from);
}

function RandomMark(count) {
    var text = "";
    var possible = "!@#$%^&*()_+{}:<>?";
    for (var i = 0; i < count; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}

function CheckUserNameExist() {
    var n = document.getElementById('register_username').value;
    console.log('\'' + n + '\'');
    if (user_data[n] != undefined) {
        // document.getElementById('register_username').value = '';
        alert('You cannot use \'' + n + '\' as Full name, Username repeated, Please use Another name.');
        // document.getElementById('register_username').focus();
        return false;
    }
    else if (n.includes(' ')) {
        // document.getElementById('register_username').value = '';
        alert('You cannot use \'' + n + '\' as Username, must not contain space, Please use Another name');
        // document.getElementById('register_username').focus();
        return false;
    }
    return true;
}

function CheckNameExist() {
    var n = document.getElementById('register_name').value;
    console.log('\'' + n + '\'');
    if (!n.includes(' ')) {
        // document.getElementById('register_name').value = '';
        alert('You cannot use \'' + n + '\' as Full name, must contain space, Please use Another name.');
        document.getElementById('register_name').focus();
        return false;
    }
    return true;
}

function checkVaildEmailOnchange() {
    var register_email = document.getElementById('register_email').value;
    if (!(register_email.includes('@') && register_email.includes('.'))) {
        alert('Your Email should Contain @ and .');
        // document.getElementById('register_email').value = '';
        return false;
    }
    var a_devide = register_email.split('@');
    if (a_devide[1].length <= 0) {
        alert('Your Email lenght should invaild before @ ');
        // document.getElementById('register_email').value = '';
        return false;
    }
    if (a_devide[0].length <= 0) {
        alert('Your Email lenght should invaild after @ ');
        // document.getElementById('register_email').value = '';
        return false;
    }
    return true;
}

last_age_input = 0;

function ageVaild() {
    var n = parseInt(document.getElementById('register_age').value);
    if (0 > n || n > 100) {
        document.getElementById('register_age').value = last_age_input + 0;
        return false;
    }
    else {
        last_age_input = n + 0;
        return true;
    }
}
// generate the user ID
function generateUserId() {
    const dateObj = new Date();
    const year = dateObj.getUTCFullYear();
    const month = dateObj.getUTCMonth() + 1; //months from 1-12
    const day = dateObj.getUTCDate();

    const hour = dateObj.getHours();
    const minute = dateObj.getMinutes();
    const seconds = dateObj.getSeconds();


    document.getElementById('register_id').value = year + hour + seconds + '' + RandomUpperString(5) + RandomLowerString(5) + RandomNumber(5, 800) + minute + day + month;
}
generateUserId();

function generateOnclick() {
const dateObj = new Date();
    const year = dateObj.getUTCFullYear();
    const month = dateObj.getUTCMonth() + 1; //months from 1-12
    const day = dateObj.getUTCDate();

    const hour = dateObj.getHours();
    const minute = dateObj.getMinutes();
    const seconds = dateObj.getSeconds();
    
    // console.log(generateOnclick.caller());
    var register_username = document.getElementById('register_username').value;
    var register_password = document.getElementById('register_password').value;
    var register_name = document.getElementById('register_name').value;
    var register_email = document.getElementById('register_email').value;
    var register_age = document.getElementById('register_age').value;
    // if (true) {
    // console.log('generate');
    // +RandomMark(5)
    generateUserId();
    document.getElementById('register_username').value = RandomUpperString(5) + RandomLowerString(5) + RandomNumber(5, 300) + year + month + day + hour + minute + seconds;
    // +RandomMark(5)
    document.getElementById('register_name').value = RandomUpperString(5) + " " + RandomLowerString(5);
    document.getElementById('register_password').value = RandomUpperString(5) + RandomLowerString(5) + RandomNumber(5, 300);

    document.getElementById('register_email').value = RandomUpperString(5) + RandomLowerString(5) + '@gmail.com';
    document.getElementById('register_age').value = RandomNumber(5, 100);
    // }
}
document.getElementById('generate_button').addEventListener('click', generateOnclick, false);

function registerOnclick() {
    console.clear();
    var av = ageVaild();
    var ec = checkVaildEmailOnchange();
    var vn = CheckNameExist();
    var unv = CheckUserNameExist();
    if (!(av && ec && vn && unv)) {
        return false;
    }
    console.log('register');
    var arr = {
        register_id: document.getElementById('register_id').value,
        username: document.getElementById('register_username').value,
        password: document.getElementById('register_password').value,
        name: document.getElementById('register_name').value,
        email: document.getElementById('register_email').value,
        age: parseInt(document.getElementById('register_age').value)
    };
    for (e in arr) {
        if (arr[e] == '') {
            alert('Your Input form have Empty.');
            return false;
        }
    }
    var json = JSON.stringify(arr);
    console.log(json);
    writeUserData(arr);
    clearRegisterData();
    generateUserId();
}

function writeUserData(arr) {
    // return new Promise(function(resolve, reject) {
        console.log('FireBase: ' + JSON.stringify(arr));
        firebase.database().ref('customer/' + arr.register_id).set(arr);
    //     return resolve();
    //     return reject();
    // });
}

function clearRegisterData() {
    // document.getElementById('register_id').value = '';
    document.getElementById('register_username').value = '';
    document.getElementById('register_password').value = '';
    document.getElementById('register_name').value = '';
    document.getElementById('register_email').value = '';
    document.getElementById('register_age').value = '';
}

function setAgeRange() {
    console.clear();
    var num = parseInt(document.getElementById('age_number').value);
    document.getElementById('age_range').value = num + 0;
    user_count = 0;
    total_age = 0;
    for (user in user_data) {
        user_count++;
        total_age += user_data[user].age;
    }
    average_age = total_age / user_count;
    console.log(total_age + ' / ' + user_count + ' = ' + average_age);
}

function setAgeNumber() {
    var num = parseInt(document.getElementById('age_range').value);
    document.getElementById('age_number').value = num + 0;
    // $scope.data = num+0;
}
