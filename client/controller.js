average_age = 0;
total_age = 0;
user_count = 0;
user_data = {};
// add angular module
var myApp = angular.module('myApp', ['firebase']);
myApp.controller('PeopleController', ["$scope", "$firebaseArray", function($scope, $firebase) {

    // Initialize Firebase
    var config = {
        apiKey: "AIzaSyDUXGTu4lkhyA4udrWFKadFSglLHPdHMuU",
        authDomain: "assignment1-52ee9.firebaseapp.com",
        databaseURL: "https://assignment1-52ee9.firebaseio.com",
        projectId: "assignment1-52ee9",
        storageBucket: "",
        messagingSenderId: "169143569218"
    };
    firebase.initializeApp(config);
    // Get a reference to the database service
    database = firebase.database().ref('customer');
    $scope.customers = $firebase(database);
    $scope.age_range = 100;
    database.on('value', onValue, errUpdateAverageAge);
    $scope.ng_UpdateAverageAge = function () {
        document.getElementById('age_number').value=parseInt($scope.age_range);
    };

    user_count = 0;
    total_age = 0;
    average_age = 0;

    function onValue(data) {
        // Firebase Promise them catch
        UpdateAverageAge(data)
            .then(function() {
                console.log('Update Average');
            })
            .catch(function() {
                console.log('Update Fall');
            });
    }

    function UpdateAverageAge(data) {
        // Firebase Promise
        return new Promise(function(resolve, reject) {
            user_count = 0;
            total_age = 0;
            average_age = 0;

            // javascript closures
            var add = (function() {
                var counter = 0;
                return function() { return counter += 1; }
            })();

            data.forEach(function(item) {
                var itemVal = item.val();
                if (itemVal.age != '' && parseInt(itemVal.age) != '') {
                    total_age += parseInt(itemVal.age);

                    // javascript closures calling
                    add();
                    user_data[itemVal.username] = itemVal;
                }
                else {
                    // Firebase Promise Reject
                    return reject();
                }
            });
            // javascript closures result
            user_count = add() - 1;
            $scope.average_age = parseInt(total_age / user_count);
            $scope.user_count = user_count + 0;
            // Firebase Promise Resolve
            return resolve();
        });
    }

    function errUpdateAverageAge(argument) {
        console.log('err' + argument);
    }

    function setElementById(id, value) {
        document.getElementById(id).value = value;
    }


    function getAllUser() {
        database.ref('/customer').once('value').then(function(snapshot) {
            function setAndShow(valname) {
                eval("var " + valname + " = (snapshot.val() && snapshot.val()." + valname + ") || 'Anonymous';console.log('" + valname + "'+" + valname + ");");
            }
            setAndShow('id');
            setAndShow('name');
            setAndShow('passsword');
            setAndShow('age');
            setAndShow('email');
        });
    }

    function getUserByID(id) {
        database.ref('/customer/' + id).once('value').then(function(snapshot) {
            function setAndShow(valname) {
                eval("var " + valname + " = (snapshot.val() && snapshot.val()." + valname + ") || 'Anonymous';console.log('" + valname + "'+" + valname + ");");
            }
            setAndShow('id');
            setAndShow('name');
            setAndShow('password');
            setAndShow('age');
            setAndShow('email');
        });
    }

    function getUserByID(id) {
        database.ref('/customer').once('value').then(function(snapshot) {
            function setAndShow(valname) {
                eval("var " + valname + " = (snapshot.val() && snapshot.val()." + valname + ") || 'Anonymous';console.log('" + valname + "'+" + valname + ");");
            }
            setAndShow('name');
            setAndShow('password');
            setAndShow('age');
            setAndShow('email');
        });
    }

    // Socket IO Connect
    var socket = io.connect();
    socket.on('connect', function() {
        document.getElementById('whole_table').style.backgroundColor = 'Skyblue';
        // alert("Socket IO Server Connected");
    });
    socket.on('message', function(msg) {
        // receive all user
        var display_all_user = document.getElementById('display_all_user');
        var arr = JSON.parse(msg);
        var begin = false;
        var table_str = '<table>';
        for (var i in arr) {
            if (!begin) {
                table_str += '<tr>';
                for (var e in arr[i]) {
                    table_str += '<td>';
                    table_str += e;
                    table_str += '</td>';

                }
                table_str += '</tr>';
                begin = true;
            }
            table_str += '<tr>';
            for (var e in arr[i]) {
                table_str += '<td>';
                table_str += arr[i][e];
                table_str += '</td>';
            }
            table_str += '</tr>';
            // display_all_user.innerHTML+='<br>';
        }
        table_str += '</table>';
        display_all_user.innerHTML = table_str;
    });
    socket.on('disconnect', function() {
        document.getElementById('whole_table').style.backgroundColor = 'red';
        alert("Socket IO Server Connected");
    });


}]);
